# Utility AI
## Description
Implementing Utility AI in Unreal Engine 4 to control the fighting behavior and to pilot the daily routine of enemy AI.

## Issue Tracking
Issue tracking via [YouTrack](https://jonaserkert.myjetbrains.com/youtrack/agiles/104-7/105-7).