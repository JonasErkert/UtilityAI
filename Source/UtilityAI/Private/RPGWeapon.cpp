// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGWeapon.h"
#include "Components/SphereComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/AnimMontage.h"
#include "DrawDebugHelpers.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "UtilityAI.h"

static int32 DebugWeaponDrawing = 0;
FAutoConsoleVariableRef CVARDebugWeaponDrawing(
	TEXT("RPG.DebugWeapon"),
	DebugWeaponDrawing,
	TEXT("Draw debug helpers of weapons"),
	ECVF_Cheat);

// Sets default values
ARPGWeapon::ARPGWeapon()
{
	// Defaults
	Damage		= 30.f;
	HitCooldown	= 0.1f;
	bHasHit		= false;
	StartTraceSocketName	= "GuardSocket";
	EndTraceSocketName		= "TipSocket";

	// Sphere as weapon root
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("Root"));
	SphereComp->InitSphereRadius(2.f);
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore); // Disable collision, just serve as a root
	RootComponent = SphereComp;

	// Capsule as simple weapon collision
	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComp"));
	CapsuleComp->InitCapsuleSize(5.f, 15.f);
	CapsuleComp->bReturnMaterialOnMove	= true; // Important to receive physical material on hit object!
	CapsuleComp->bTraceComplexOnMove	= true;
	CapsuleComp->SetupAttachment(SphereComp);

	// Weapon mesh representation
	SkeletalMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));
	SkeletalMeshComp->SetupAttachment(SphereComp);

	// Players can walk on it
	CapsuleComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Default, 0.f));
	CapsuleComp->CanCharacterStepUpOn = ECB_Yes;
}

void ARPGWeapon::BeginPlay()
{
	Super::BeginPlay();

}

void ARPGWeapon::StartWeaponDamageTracing()
{
	// Trace along the sword
	FVector StartTracePoint	= SkeletalMeshComp->GetSocketLocation(StartTraceSocketName);
	FVector EndTracePoint	= SkeletalMeshComp->GetSocketLocation(EndTraceSocketName);
	UWorld* World = GetWorld();

	if (DebugWeaponDrawing)
	{
		DrawDebugLine(World, StartTracePoint, EndTracePoint, FColor::Purple, false, 1.f, 0, 1.f);
	}

	// TODO: Move to top to improve performance when debugging is finished
	if (bHasHit)
	{
		return;
	}

	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(this);
	QueryParams.AddIgnoredActor(GetOwner());
	QueryParams.bTraceComplex = true;
	QueryParams.bReturnPhysicalMaterial = true;

	if (World)
	{
		FHitResult Hit;
		EPhysicalSurface SurfaceType = SurfaceType_Default;

		if (World->LineTraceSingleByChannel(Hit, StartTracePoint, EndTracePoint, COLLISION_WEAPON, QueryParams))
		{
			// Trace has hit something, no more line traces
			bHasHit = true;
			GetWorldTimerManager().SetTimer(TimerHandle_ResetHasHit, this, &ARPGWeapon::StartDamageCooldown, HitCooldown, false);

			AActor* HitActor = Hit.GetActor();

			SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

			AActor* WeaponOwner = GetOwner();

			// TODO: Add different reaction to surface type
		
			if (WeaponOwner)
			{
				UGameplayStatics::ApplyPointDamage(HitActor, Damage, Hit.ImpactNormal, Hit, WeaponOwner->GetInstigatorController(), WeaponOwner, DamageType);
			}

			UGameplayStatics::SpawnEmitterAtLocation(World, HitEffect, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());

			if (DebugWeaponDrawing)
			{
				DrawDebugLine(World, StartTracePoint, EndTracePoint, FColor::Red, false, 1.f, 0, 1.f);
				DrawDebugSphere(World, Hit.ImpactPoint, 20.f, 12, FColor::Green, false, 1.f, 0, 1.f);
			}
		}
	}
}

void ARPGWeapon::StartDamageCooldown()
{
	bHasHit = false;
}
