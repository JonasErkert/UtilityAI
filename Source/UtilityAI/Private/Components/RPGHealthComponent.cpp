// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGHealthComponent.h"

// Sets default values for this component's properties
URPGHealthComponent::URPGHealthComponent()
{
	// Defaults
	DefaultHealth = 100.f;
}


// Called when the game starts
void URPGHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// Handle damage events
	AActor* ComponentOwner = GetOwner();
	if (ComponentOwner)
	{
		ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &URPGHealthComponent::HandleTakeAnyDamage);
	}

	Health = DefaultHealth;
}

void URPGHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.f)
	{
		return;
	}

	// Clamp health when updated
	Health = FMath::Clamp(Health - Damage, 0.f, DefaultHealth);

	UE_LOG(LogTemp, Log, TEXT("Health changed: %s"), *FString::SanitizeFloat(Health));

	OnHealthChanged.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);
}

void URPGHealthComponent::Heal(float Amount, class AController* HealInstigator, AActor* HealCauser)
{
	Health = FMath::Clamp(Health + Amount, 0.f, DefaultHealth);

	const UDamageType* DamageType = nullptr;
	OnHealthChanged.Broadcast(this, Health, Amount, DamageType, HealInstigator, HealCauser);
}
