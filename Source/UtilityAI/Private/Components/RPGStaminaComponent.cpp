// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGStaminaComponent.h"

// Sets default values for this component's properties
URPGStaminaComponent::URPGStaminaComponent()
{
	// Defaults
	DefaultStamina = 100.f;
}


// Called when the game starts
void URPGStaminaComponent::BeginPlay()
{
	Super::BeginPlay();

	Stamina = DefaultStamina;
}

void URPGStaminaComponent::UpdateStamina(float Amount)
{
	Stamina = FMath::Clamp(Stamina + Amount, 0.f, DefaultStamina);

	if (Stamina <= 0.f)
	{
		OnStaminaDepletedReplenished.Broadcast(this, false);
		UE_LOG(LogTemp, Log, TEXT("Stamina fully depleted!"));
	}
	
	if (Stamina >= 100.f)
	{
		OnStaminaDepletedReplenished.Broadcast(this, true);
		UE_LOG(LogTemp, Log, TEXT("Stamina fully replenished!"));
	}
}

float URPGStaminaComponent::GetStamina() const
{
	return Stamina;
}

float URPGStaminaComponent::GetDefaultStamina() const
{
	return DefaultStamina;
}
