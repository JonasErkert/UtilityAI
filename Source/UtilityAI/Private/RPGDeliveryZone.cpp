// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGDeliveryZone.h"
#include <Components/BoxComponent.h>
#include "RPGCharacterBase.h"
#include "RPGGameModeBase.h"

// Sets default values
ARPGDeliveryZone::ARPGDeliveryZone()
{
	OverlapComp = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComp"));
	OverlapComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	OverlapComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	OverlapComp->SetBoxExtent(FVector(200.f));

	RootComponent = OverlapComp;

	OverlapComp->OnComponentBeginOverlap.AddDynamic(this, &ARPGDeliveryZone::HandleOverlap);
}

void ARPGDeliveryZone::HandleOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ARPGCharacterBase* MyPawn = Cast<ARPGCharacterBase>(OtherActor);

	if (MyPawn && MyPawn->bIsCarryingObjective)
	{
		ARPGGameModeBase* GameModeBase = Cast<ARPGGameModeBase>(GetWorld()->GetAuthGameMode());

		if (GameModeBase)
		{
			GameModeBase->CompleteMission(MyPawn);
		}
	}
}

