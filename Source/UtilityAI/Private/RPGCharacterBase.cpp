// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGCharacterBase.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "UtilityAI.h"
#include "RPGStaminaComponent.h"
#include "RPGHealthComponent.h"
#include "RPGRangedWeapon.h"
#include "RPGWeapon.h"
#include <Kismet/GameplayStatics.h>

// Sets default values
ARPGCharacterBase::ARPGCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Defaults
	CurrentCombatMode	= ECombatMode::ENone;
	RotationRate		= 540.f;
	MaxRunningSpeed		= 600.f;
	MaxCrouchSpeed		= 500.f;
	bDied				= false;
	RunningStaminaConsumption		= 1.f;
	FullyChargedStaminaConsumption	= 1.f;
	IdleStaminaRegeneration			= 2.f;
	LeftHandSocketName	= "LeftHandSocket";
	HandShieldSocketName= "HandShieldSocket";
	RightHandSocketName	= "RightHandSocket";
	QuiverSocketName	= "QuiverSocket";
	BowSocketName		= "BowSocket";
	SwordSocketName		= "SwordSocket";
	DaggerSocketName	= "DaggerSocket";
	ShieldSocketName	= "ShieldSocket";
	AimCamPosition		= FVector(190.f, 85.f, 0.f);
	CamTransitionSpeed	= 1.f;
	bShouldTransition	= false;
	bAiming				= false;
	bIsShieldBlocking	= false;
	bMoveToAimPosition	= false;

	bSmallWeaponEquipped	= false;
	bWeaponEquipped			= false;
	bShieldEquipped			= false;
	bRangedWeaponEquipped	= false;

	bIsCarryingObjective = false;

	// Spring arm component
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->bUsePawnControlRotation = true;
	SpringArmComp->SetupAttachment(RootComponent);

	// Third person camera
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp);

	// Health component
	HealthComp = CreateDefaultSubobject<URPGHealthComponent>(TEXT("HealthComp"));

	// Stamina component
	StaminaComp = CreateDefaultSubobject<URPGStaminaComponent>(TEXT("StaminaComp"));

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;

	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Ignore);

	// Character movement configuration
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, RotationRate, 0.f);
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch	= false;
	bUseControllerRotationYaw	= false;
	bUseControllerRotationRoll	= false;
}

// Called when the game starts or when spawned
void ARPGCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	
	MaxWalkSpeedDefault		= GetCharacterMovement()->MaxWalkSpeed;
	MaxCrouchSpeedDefault	= GetCharacterMovement()->MaxWalkSpeedCrouched;

	HealthComp->OnHealthChanged.AddDynamic(this, &ARPGCharacterBase::OnHealthChanged);
	StaminaComp->OnStaminaDepletedReplenished.AddDynamic(this, &ARPGCharacterBase::OnStaminaDepletedReplenished);

	// Attach all weapons to the back of the character
	UWorld* World = GetWorld();
	if (World)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		// Ranged weapon
		CurrentRangedWeapon = World->SpawnActor<ARPGRangedWeapon>(EquippedRangedWeapon, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (CurrentRangedWeapon)
		{
			CurrentRangedWeapon->SetOwner(this);
			CurrentRangedWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, BowSocketName);
			CurrentRangedWeapon->OnFullyCharged.AddDynamic(this, &ARPGCharacterBase::OnChargeExhaustion); // Player held charging for too long, use up stamina
		}

		// Weapon
		CurrentWeapon = World->SpawnActor<ARPGWeapon>(EquippedWeapon, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (CurrentWeapon)
		{
			CurrentWeapon->SetOwner(this);
			CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SwordSocketName);
		}

		// Small weapon
		CurrentSmallWeapon = World->SpawnActor<ARPGWeapon>(EquippedSmallWeapon, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (CurrentSmallWeapon)
		{
			CurrentSmallWeapon->SetOwner(this);
			CurrentSmallWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, DaggerSocketName);
		}

		// Quiver
		CurrentQuiver = World->SpawnActor<AActor>(EquippedQuiver, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (CurrentQuiver)
		{
			CurrentQuiver->SetOwner(this);
			CurrentQuiver->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, QuiverSocketName);
		}

		// Shield
		CurrentShield = World->SpawnActor<AActor>(EquippedShield, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (CurrentShield)
		{
			CurrentShield->SetOwner(this);
			CurrentShield->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, ShieldSocketName);
		}
	}

	// Set camera default positions
	DefaultCamPosition = FVector::ZeroVector; // Zero vector because camera is attached to the spring arm
	CurrentCamPosition = DefaultCamPosition;

	// Set default combat state
	switch (CurrentCombatMode)
	{
	case ECombatMode::ENone:
		OnEnterNoneMode();
		break;
	case ECombatMode::EStealth:
		ToggleStealthMode();
		break;
	case ECombatMode::EMelee:
		ToggleMeleeMode();
		break;
	case ECombatMode::ERanged:
		ToggleRangedMode();
		break;
	}
}

// Called every frame
void ARPGCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Move the camera to the aiming location or back
	if (bShouldTransition)
	{
		if (bMoveToAimPosition)
		{
			MoveCamToAimPos(DeltaTime);
		}
		else
		{
			MoveCamToDefaultPos(DeltaTime);
		}
	}
}

// Called to bind functionality to input
void ARPGCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	// Axis input
	PlayerInputComponent->BindAxis("MoveForward", this, &ARPGCharacterBase::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ARPGCharacterBase::MoveRight);

	PlayerInputComponent->BindAxis("LookUp", this, &ARPGCharacterBase::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &ARPGCharacterBase::AddControllerYawInput);

	// Action input
	PlayerInputComponent->BindAction("Space", IE_Pressed, this, &ARPGCharacterBase::SpacebarInputActionStart);
	PlayerInputComponent->BindAction("Space", IE_Released, this, &ARPGCharacterBase::SpacebarInputActionStop);

	PlayerInputComponent->BindAction("Shift", IE_Pressed, this, &ARPGCharacterBase::ShiftInputActionStart);
	PlayerInputComponent->BindAction("Shift", IE_Released, this, &ARPGCharacterBase::ShiftInputActionStop);

	PlayerInputComponent->BindAction("StealthMode",	IE_Released, this, &ARPGCharacterBase::ToggleStealthMode);
	PlayerInputComponent->BindAction("MeleeMode",	IE_Released, this, &ARPGCharacterBase::ToggleMeleeMode);
	PlayerInputComponent->BindAction("RangedMode",	IE_Released, this, &ARPGCharacterBase::ToggleRangedMode);

	PlayerInputComponent->BindAction("Right", IE_Pressed, this, &ARPGCharacterBase::RightClickInputActionStart);
	PlayerInputComponent->BindAction("Right", IE_Released, this, &ARPGCharacterBase::RightClickInputActionStop);

	PlayerInputComponent->BindAction("Left", IE_Pressed, this, &ARPGCharacterBase::LeftClickInputAction);
}

void ARPGCharacterBase::MoveForward(float Value)
{
	// If bUseControllerRotationYaw is true, the character turns away from the camera
	if (!bUseControllerRotationYaw)
	{
		if ((Controller != nullptr) && (Value != 0.f))
		{
			// Get character yaw rotation
			FRotator ControlRot = Controller->GetControlRotation();
			FRotator YawRotation(0.f, ControlRot.Yaw, 0.f);

			// Calculate forward vector
			FVector CharacterDir = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
			AddMovementInput(CharacterDir, Value);
		}
	}
	else
	{
		AddMovementInput(GetActorForwardVector() * Value);
	}
}

void ARPGCharacterBase::MoveRight(float Value)
{
	// If bUseControllerRotationYaw is true, the character turns away from the camera
	if (!bUseControllerRotationYaw)
	{
		if ((Controller != nullptr) && (Value != 0.f))
		{
			// Get character yaw rotation
			FRotator ControlRot = Controller->GetControlRotation();
			FRotator YawRotation(0.f, ControlRot.Yaw, 0.f);

			// Calculate right vector
			FVector CharacterDir = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
			AddMovementInput(CharacterDir, Value);
		}
	}
	else
	{
		AddMovementInput(GetActorRightVector() * Value);
	}
}

void ARPGCharacterBase::SwitchRotationMode(bool bIsFreeRotation)
{
	// Toggle rotation mode
	bUseControllerRotationYaw = bIsFreeRotation;
}

void ARPGCharacterBase::LeftClickInputAction()
{
	switch (CurrentCombatMode)
	{
	case ECombatMode::ENone:
		break;
	case ECombatMode::EStealth:
		// Perform a stealth attack
		SmallWeaponAttack();
		break;
	case ECombatMode::EMelee:
		// Perform a light attack
		WeaponLightAttack();
		// TODO: When button is hold for more than ~0.5 seconds, perform a heavy attack
		break;
	case ECombatMode::ERanged:
		// Shoot the projectile
		ShootRangedWeapon();
		break;
	}
}

void ARPGCharacterBase::RightClickInputActionStart()
{
	switch (CurrentCombatMode)
	{
	case ECombatMode::ENone:
		break;
	case ECombatMode::EStealth:
		// Do nothing
		break;
	case ECombatMode::EMelee:
		// Start holding the shield in block position
		OnStartShieldBlock();
		break;
	case ECombatMode::ERanged:
		StopRunning();
		StartAiming();
		break;
	}
}

void ARPGCharacterBase::RightClickInputActionStop()
{
	switch (CurrentCombatMode)
	{
	case ECombatMode::ENone:
		break;
	case ECombatMode::EStealth:
		// Do nothing
		break;
	case ECombatMode::EMelee:
		// Stop holding the shield in block position
		OnStopShieldBlock();
		break;
	case ECombatMode::ERanged:
		// Stop aiming
		StopAiming();
		break;
	}
}

void ARPGCharacterBase::SpacebarInputActionStart()
{
	switch (CurrentCombatMode)
	{
	case ECombatMode::ENone:
		break;
	case ECombatMode::EStealth:
		break;
	case ECombatMode::EMelee:
		// Stop blocking when the character is jumping
		if (bIsShieldBlocking)
		{
			OnStopShieldBlock();
		}
		break;
	case ECombatMode::ERanged:
		// Stop aiming when character is jumping
		if (bAiming)
		{
			StopAiming();
		}
		break;
	}

	Jump();
}

void ARPGCharacterBase::SpacebarInputActionStop()
{
	StopJumping();
	
	switch (CurrentCombatMode)
	{
	case ECombatMode::ENone:
		break;
	case ECombatMode::EStealth:
		break;
	case ECombatMode::EMelee:
		break;
	case ECombatMode::ERanged:
		break;
	}
}

void ARPGCharacterBase::ShiftInputActionStart()
{
	bool bCanRun = true;

	switch (CurrentCombatMode)
	{
	case ECombatMode::ENone:
		break;
	case ECombatMode::EStealth:
		bCanRun = true;
		break;
	case ECombatMode::EMelee:
		// Character can't run if blocking with a shield
		if (bIsShieldBlocking)
		{
			bCanRun = false;
		}
		break;
	case ECombatMode::ERanged:
		// Character can't run if aiming
		if (bAiming)
		{
			bCanRun = false;
		}
		break;
	}

	if (bCanRun)
	{
		Run();
	}
}

void ARPGCharacterBase::ShiftInputActionStop()
{
	bool bCanRun = true;

	switch (CurrentCombatMode)
	{
	case ECombatMode::ENone:
		break;
	case ECombatMode::EStealth:
		break;
	case ECombatMode::EMelee:
		// Character can't run if blocking with a shield
		if (bIsShieldBlocking)
		{
			bCanRun = false;
		}
		break;
	case ECombatMode::ERanged:
		// Character can't run if aiming
		if (bAiming)
		{
			bCanRun = false;
		}
		break;
	}

	if (bCanRun)
	{
		StopRunning();
	}
}

void ARPGCharacterBase::ToggleStealthMode()
{
	SwitchToCombatState(ECombatMode::EStealth);

	Crouch();
}

void ARPGCharacterBase::ToggleMeleeMode()
{
	SwitchToCombatState(ECombatMode::EMelee);
}

void ARPGCharacterBase::ToggleRangedMode()
{
	SwitchToCombatState(ECombatMode::ERanged);
}

void ARPGCharacterBase::OnEnterNoneMode_Implementation()
{
	// Stop crouching
	if (bIsCrouched)
	{
		UnCrouch();
	}

	// Stop aiming
	if (bAiming)
	{
		// Stop aiming
		StopAiming();
		AimingChanged();
	}
}

void ARPGCharacterBase::SwitchToCombatState(ECombatMode CombatMode)
{
	// Call this no matter the combat state to prepare switching to other modes.
	// E.g. from sword to bow mode: character has to first sheath the sword to draw the bow.
	// Has to be called before setting the CurrentCombatMode, because some abilities work only in specific modes.
	OnEnterNoneMode();

	// If the character is already in this state, switch to combat state none to sheath weapons.
	if (CurrentCombatMode == CombatMode)
	{
		CurrentCombatMode = ECombatMode::ENone;
	}
	else
	{
		CurrentCombatMode = CombatMode;
	}

	// Call BP event to update the UI and to equip the weapons
	CombatModeChanged();
}

void ARPGCharacterBase::Run()
{
	GetCharacterMovement()->MaxWalkSpeed = MaxRunningSpeed;
	GetCharacterMovement()->MaxWalkSpeedCrouched = MaxCrouchSpeed;

	// Clear stamina regeneration timer to avoid consumption/ regeneration at the same time
	GetWorldTimerManager().ClearTimer(TimerHandle_StaminaRegeneration);

	// Decrease stamina while character is running
	GetWorldTimerManager().SetTimer(TimerHandle_RunningStaminaConsumption, this, &ARPGCharacterBase::ConsumeStaminaRun, 0.05f, true);
}

void ARPGCharacterBase::StopRunning()
{
	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeedDefault;
	GetCharacterMovement()->MaxWalkSpeedCrouched = MaxCrouchSpeedDefault;

	// Clear stamina consumption timer to avoid consumption/ regeneration at the same time
	GetWorldTimerManager().ClearTimer(TimerHandle_RunningStaminaConsumption);

	// Player has stopped running, regenerate stamina
	RegenerateStaminaOverTime();
}

void ARPGCharacterBase::ConsumeStamina(float Amount)
{
	// Negative to reduce stamina amount
	// StaminaComp validated in previous functions
	StaminaComp->UpdateStamina(-Amount);
}

void ARPGCharacterBase::ConsumeStaminaRun()
{
	if (StaminaComp)
	{
		// Check if the character is running to avoid stamina drain when only the key is pressed, but the character doesn't move
		bool bIsRunning = GetCharacterMovement()->Velocity.Size() > MaxWalkSpeedDefault;
		if (!bIsRunning)
		{
			// Don't consume stamina if the character is standing or walking
			return;
		}

		// Make the character walk if stamina is zero
		if (StaminaComp->GetStamina() <= 0.f)
		{
			StopRunning();
			return;
		}

		ConsumeStamina(RunningStaminaConsumption);
	}
}

void ARPGCharacterBase::ConsumeStaminaCharge()
{
	if (StaminaComp)
	{
		ConsumeStamina(FullyChargedStaminaConsumption);
	}
}

void ARPGCharacterBase::RegenerateStamina()
{
	if (StaminaComp)
	{
		// TODO: There are events for that
		if (StaminaComp->GetStamina() >= StaminaComp->GetDefaultStamina())
		{
			// Stamina is full, stop regenerating
			GetWorldTimerManager().ClearTimer(TimerHandle_StaminaRegeneration);
			return;
		}

		StaminaComp->UpdateStamina(IdleStaminaRegeneration);
	}
}

void ARPGCharacterBase::RegenerateStaminaOverTime()
{
	GetWorldTimerManager().SetTimer(TimerHandle_StaminaRegeneration, this, &ARPGCharacterBase::RegenerateStamina, 0.05f, true);
}

void ARPGCharacterBase::OnHealthChanged(URPGHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health <= 0.f && !bDied)
	{
		// Die
		bDied = true;

		CharacterDied();

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		// This will stop the dead body from rotating
		//DetachFromControllerPendingDestroy();

		// Remove dead body from level
		SetLifeSpan(10.f);
	}
}

void ARPGCharacterBase::ShootRangedWeapon()
{
	if (CurrentCombatMode == ECombatMode::ERanged && bAiming)
	{
		if (CurrentRangedWeapon)
		{
			CurrentRangedWeapon->Shoot();

			// Reset charging, then start charging again immediately
			StopChargeRangedWeapon();
			ChargeRangedWeapon();
		}
	}
}

void ARPGCharacterBase::OnStartShieldBlock_Implementation()
{
	bIsShieldBlocking = true;
}

void ARPGCharacterBase::OnStopShieldBlock_Implementation()
{
	bIsShieldBlocking = false;
}

void ARPGCharacterBase::EquipSmallWeapon(bool bEquip /*= true*/)
{
	FName SocketName = DaggerSocketName;
	bool bEquipped = false;
	if (bEquip)
	{
		SocketName = RightHandSocketName;
		bEquipped = true;
	}

	if (CurrentSmallWeapon)
	{
		CurrentSmallWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketName);
	}

	bSmallWeaponEquipped = bEquipped;
}

void ARPGCharacterBase::EquipWeapon(bool bEquip /*= true*/)
{
	FName SocketName = SwordSocketName;
	bool bEquipped = false;
	if (bEquip)
	{
		SocketName = RightHandSocketName;
		bEquipped = true;
	}

	if (CurrentWeapon)
	{
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketName);
	}

	bWeaponEquipped = bEquipped;
}

void ARPGCharacterBase::EquipShield(bool bEquip /*= true*/)
{
	FName SocketName = ShieldSocketName;
	bool bEquipped = false;
	if (bEquip)
	{
		SocketName = HandShieldSocketName;
		bEquipped = true;
	}

	if (CurrentShield)
	{
		CurrentShield->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketName);
	}

	bShieldEquipped = bEquipped;
}

void ARPGCharacterBase::EquipRangedWeapon(bool bEquip /*= true*/)
{
	FName SocketName = BowSocketName;
	bool bEquipped = false;
	if (bEquip)
	{
		SocketName = LeftHandSocketName;
		bEquipped = true;
	}

	if (CurrentRangedWeapon)
	{
		CurrentRangedWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketName);
	}

	bRangedWeaponEquipped = bEquipped;
}

void ARPGCharacterBase::StartAiming()
{
	if (CurrentCombatMode == ECombatMode::ERanged)
	{
		SwitchRotationMode(true);

		bShouldTransition	= true;
		bMoveToAimPosition	= true;
		bAiming				= true;
		ChargeRangedWeapon();

		AimingChanged();
	}
}

void ARPGCharacterBase::StopAiming()
{
	if (CurrentCombatMode == ECombatMode::ERanged)
	{
		SwitchRotationMode(false);

		bShouldTransition	= true;
		bMoveToAimPosition	= false;
		bAiming				= false;
		StopChargeRangedWeapon();

		AimingChanged();
	}
}

void ARPGCharacterBase::ChargeRangedWeapon()
{
	if (CurrentRangedWeapon)
	{
		CurrentRangedWeapon->ChargeShot();
	}
}

void ARPGCharacterBase::StopChargeRangedWeapon()
{
	if (CurrentRangedWeapon)
	{
		CurrentRangedWeapon->StopChargeShot();
	}

	GetWorldTimerManager().ClearTimer(TimerHandle_FullyChargedStaminaConsumption);

	RegenerateStaminaOverTime();
}

void ARPGCharacterBase::OnChargeExhaustion(ARPGRangedWeapon* RangedWeapon)
{
	// Start reducing stamina
	GetWorldTimerManager().SetTimer(TimerHandle_FullyChargedStaminaConsumption, this, &ARPGCharacterBase::ConsumeStaminaCharge, 0.05f, true);

	// Also clear possible stamina regeneration
	GetWorldTimerManager().ClearTimer(TimerHandle_StaminaRegeneration);

	// Shake the camera when exhausted
	// TODO: Increase inaccuracy when player has low stamina
	ShakeCamera();

	// TODO: Maybe play effects like heavy breathing
}

void ARPGCharacterBase::OnStaminaDepletedReplenished(URPGStaminaComponent* OwningStaminaComp, bool bIsMax)
{
	// If the stamina is depleted stop aiming!
	if (!bIsMax)
	{
		StopAiming();
	}
}

void ARPGCharacterBase::ShakeCamera()
{
	APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (PC)
	{
		PC->ClientPlayCameraShake(ShootCameraShake);
	}
}

void ARPGCharacterBase::MoveCamToAimPos(float DeltaTime)
{
	CurrentCamPosition = FMath::VInterpTo(CurrentCamPosition, AimCamPosition, DeltaTime, CamTransitionSpeed);
	GetCameraComp()->SetRelativeLocation(CurrentCamPosition);

	if (CurrentCamPosition.Equals(AimCamPosition))
	{
		bShouldTransition = false;
	}
}

void ARPGCharacterBase::MoveCamToDefaultPos(float DeltaTime)
{
	CurrentCamPosition = FMath::VInterpTo(CurrentCamPosition, DefaultCamPosition, DeltaTime, CamTransitionSpeed);
	GetCameraComp()->SetRelativeLocation(CurrentCamPosition);

	if (CurrentCamPosition.Equals(DefaultCamPosition))
	{
		bShouldTransition = false;
	}
}

ARPGRangedWeapon* ARPGCharacterBase::GetRangedWeapon() const
{
	return CurrentRangedWeapon;
}

FVector ARPGCharacterBase::GetPawnViewLocation() const
{
	if (CameraComp)
	{
		return CameraComp->GetComponentLocation();
	}

	return Super::GetPawnViewLocation();
}

