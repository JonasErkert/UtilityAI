// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGProjectile.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "DrawDebugHelpers.h"
#include "UtilityAI.h"
#include "RPGRangedWeapon.h"

static int32 DebugProjectileDrawing = 0;
FAutoConsoleVariableRef CVARDebugProjectileDrawing(
	TEXT("RPG.DebugProjectile"),
	DebugProjectileDrawing,
	TEXT("Draw debug helpers of projectiles"),
	ECVF_Cheat);

// Sets default values
ARPGProjectile::ARPGProjectile()
{
	// Defaults
	InitialSpeed	= 3000.f;
	MaxSpeed		= 3000.f;
	LifeSpan		= 10.f;
	BaseDamage		= 20.f;
	SpawnedAtLocation = FVector::ZeroVector;

	// Sphere as a simple collision
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->InitSphereRadius(2.f);
	SphereComp->bReturnMaterialOnMove = true; // Important to receive physical material on hit object!
	SphereComp->bTraceComplexOnMove = true;

	// Players can walk on it
	SphereComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Default, 0.f));
	SphereComp->CanCharacterStepUpOn = ECB_Yes;

	// Collision settings
	SphereComp->BodyInstance.SetCollisionProfileName("Projectile");
	SphereComp->SetCollisionObjectType(COLLISION_PROJECTILE);

	RootComponent = SphereComp;

	// Projectile mesh
	ProjectileMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMeshComp"));
	ProjectileMeshComp->SetupAttachment(SphereComp);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovement->UpdatedComponent		= SphereComp;
	ProjectileMovement->InitialSpeed			= InitialSpeed;
	ProjectileMovement->MaxSpeed				= MaxSpeed;
	ProjectileMovement->bShouldBounce			= false;
	ProjectileMovement->bRotationFollowsVelocity= true;

	InitialLifeSpan = LifeSpan;
}

// Called when the game starts or when spawned
void ARPGProjectile::BeginPlay()
{
	Super::BeginPlay();

	SphereComp->OnComponentHit.AddDynamic(this, &ARPGProjectile::OnHit);
}

float ARPGProjectile::CalculatedDamage(bool bIsHeadshot)
{
	if (ProjectileSpawnerWeapon == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Projectile spawner not passed properly!"));
		return 0.f;
	}

	// Multiply BaseDamage if hit is headshot
	float DamageDealt = bIsHeadshot ? BaseDamage * ProjectileSpawnerWeapon->GetHeadShotMultiplier() : BaseDamage;
	float ChargeAmount = ProjectileSpawnerWeapon->GetChargeAmount();

	// And add damage based on the charge amount
	if (ChargeAmount < 26.f)
	{
		// Do not increase damage if the weapon is not charged
	}
	else if (ChargeAmount < 51.f)
	{
		DamageDealt += BaseDamage * 0.25f;
	}
	else if (ChargeAmount < 76.f)
	{
		DamageDealt += BaseDamage * 0.5f;
	}
	else
	{
		DamageDealt += BaseDamage * 0.75f;
	}

	return DamageDealt;
	
}

void ARPGProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if ((OtherActor != nullptr)	&&
		(OtherActor != this)	&&
		//(OtherActor != ProjectileSpawnerWeapon->GetOwner()) &&
		ProjectileSpawnerWeapon != nullptr)
	{
		// Process damage
		AActor* HitActor = Hit.GetActor();

		AActor* RangedWeaponOwner = ProjectileSpawnerWeapon->GetOwner();

		EPhysicalSurface SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

		bool bHitHead = false;
		if (SurfaceType == SURFACE_IMPACT_FLESH)
		{
			bHitHead = true;
		}

		UGameplayStatics::ApplyPointDamage(HitActor, CalculatedDamage(bHitHead), Hit.ImpactNormal, Hit, RangedWeaponOwner->GetInstigatorController(), RangedWeaponOwner, DamageType);

		UParticleSystem* SelectedEffect = nullptr;
		switch (SurfaceType)
		{
		case SURFACE_IMPACT_DEFAULT:
			SelectedEffect = DefaultImpactEffect;
			break;
		case SURFACE_IMPACT_FLESH:
			SelectedEffect = FleshImpactEffect;
			break;
		default:
			SelectedEffect = DefaultImpactEffect;
			break;
		}

		if (SelectedEffect)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SelectedEffect, Hit.ImpactPoint, Hit.ImpactNormal.Rotation());
		}

		// TODO: Attach projectile to other actor

		// Add impulse if a physics body was hit
		if (OtherComp->IsSimulatingPhysics())
		{
			OtherComp->AddImpulseAtLocation(GetVelocity() * 100.f, GetActorLocation());

			if (DebugProjectileDrawing)
			{
				UE_LOG(LogTemp, Log, TEXT("Projectile hit physics body!"));
			}
		}

		if (DebugProjectileDrawing)
		{
			DrawDebugSphere(GetWorld(), Hit.ImpactPoint, 5.f, 12, FColor::Red, false, 5.f, 0, 1.f);
			DrawDebugLine(GetWorld(), Hit.ImpactPoint, SpawnedAtLocation, FColor::Green, false, 5.f, 0, 1.f);
		}

		// TODO: Remove this when attach projectile (see todo above) is implemented or delay destroy with SetLifeSpan(?)
		Destroy();
	}
}

