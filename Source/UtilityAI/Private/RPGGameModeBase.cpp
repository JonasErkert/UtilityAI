// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGGameModeBase.h"

ARPGGameModeBase::ARPGGameModeBase()
{
}

void ARPGGameModeBase::CompleteMission(APawn* InstigatorPawn)
{
	if (InstigatorPawn)
	{
		InstigatorPawn->DisableInput(nullptr);
	}

	OnMissionCompleted(InstigatorPawn);
}

void ARPGGameModeBase::GameOver()
{
	OnPlayerDeath();
}
