// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGRangedWeapon.h"
#include <Components/SkeletalMeshComponent.h>
#include "RPGProjectile.h"
#include "Animation/AnimMontage.h"
#include "DrawDebugHelpers.h"
#include "RPGCharacterBase.h"

static int32 DebugRangedWeapon = 0;
FAutoConsoleVariableRef CVARDebugRangedWeaponDrawing(
	TEXT("RPG.DebugRangedWeapon"),
	DebugRangedWeapon,
	TEXT("Draw debug helpers of ranged weapons"),
	ECVF_Cheat);

// Sets default values
ARPGRangedWeapon::ARPGRangedWeapon()
{
	// Defaults
	GripSocketName		= "Grip";
	HeadShotMultiplier	= 2.f;
	ChargeAmount		= 0.f;
	ChargeRate			= 12.5f;
	ProjectileOffset	= FVector(100.f, 0.f, 0.f);
	MaxAmmunition		= 20;
	CurrentAmmunition	= 20;
	ProjectileDirectionOffset = 10000.f;

	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;
}

void ARPGRangedWeapon::Shoot()
{
	if (CurrentAmmunition <= 0)
	{
		return;
	}

	// Reduce ammo count
	CurrentAmmunition--;

	// Trace the world, from pawn eyes to crosshair location
	AActor* WeaponOwner = GetOwner();

	if (WeaponOwner)
	{
		// Shoot projectile
		if (ProjectileToShoot)
		{
			UWorld* const World = GetWorld();
			if (World)
			{
				FVector EyeLocation;
				FRotator EyeRotation;
				WeaponOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

				FVector EyeDirection = EyeRotation.Vector();

				FCollisionQueryParams QueryParams;
				QueryParams.AddIgnoredActor(WeaponOwner);
				QueryParams.AddIgnoredActor(this);

				FVector ShotEnd = EyeLocation + (EyeDirection * ProjectileDirectionOffset);
				FVector SpawnLocation	= MeshComp->GetSocketLocation(GripSocketName) + WeaponOwner->GetActorForwardVector() * ProjectileOffset/*EyeRotation.RotateVector(ProjectileOffset)*/;
	
				FHitResult Hit;
				if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, ShotEnd, ECC_Camera, QueryParams))
				{
					ShotEnd = Hit.ImpactPoint;
				}

				FVector SpawnDirection	= (ShotEnd - SpawnLocation);
				SpawnDirection.Normalize();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

				ARPGProjectile* SpawnedProjectile = World->SpawnActor<ARPGProjectile>(ProjectileToShoot, SpawnLocation, SpawnDirection.ToOrientationRotator(), SpawnParams);
				SpawnedProjectile->ProjectileSpawnerWeapon = this;
				SpawnedProjectile->SpawnedAtLocation = SpawnLocation;

				if (DebugRangedWeapon)
				{
					DrawDebugSphere(World, ShotEnd, 10.f, 12, FColor::Red, false, 1.f, 0, 1.f);
					DrawDebugLine(World, SpawnLocation, ShotEnd, FColor::Green, false, 1.f, 0, 1.f);
					DrawDebugLine(World, EyeLocation, ShotEnd, FColor::Purple, false, 1.f, 0, 1.f);
				}
			}
		}

		// TODO: Play sound
	}
}

void ARPGRangedWeapon::StartCharging()
{
	ChargeAmount = FMath::Clamp(ChargeAmount + ChargeRate, 0.f, 100.f);
	
	// 100 is when the player held more than 2 seconds
	if (ChargeAmount >= 100.f)
	{
		OnFullyCharged.Broadcast(this);
		UE_LOG(LogTemp, Log, TEXT("Weapon fully charged!"));
	}
}

void ARPGRangedWeapon::UpdateAmmunitionAmount(int32 Amount)
{
	CurrentAmmunition = FMath::Clamp(CurrentAmmunition + Amount, 0, MaxAmmunition);
}

void ARPGRangedWeapon::ChargeShot()
{
	GetWorldTimerManager().SetTimer(TimerHandle_ChargeShot, this, &ARPGRangedWeapon::StartCharging, 0.25f, true, 0.f);
}

void ARPGRangedWeapon::StopChargeShot()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_ChargeShot);
	ChargeAmount = 0.f;
}

float ARPGRangedWeapon::GetHeadShotMultiplier() const
{
	return HeadShotMultiplier;
}

float ARPGRangedWeapon::GetChargeAmount() const
{
	return ChargeAmount;
}
