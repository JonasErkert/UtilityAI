// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RPGRangedWeapon.generated.h"

class USkeletalMeshComponent;
class ARPGProjectile;
class UAnimMontage;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnFullyChargedSignature, ARPGRangedWeapon*, RangedWeapon);

UCLASS()
class UTILITYAI_API ARPGRangedWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARPGRangedWeapon();

protected:
	/** The mesh which represents the ranged weapon, e.g. a bow. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* MeshComp;

	/** Name of the socket to attach effects or a projectile. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName GripSocketName;

	/** Multiplies the base damage with the specified amount. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float HeadShotMultiplier;

	/**
	 * Defines the amount, in which the weapon is charged (hold).
	 * Increases over time depending on the charge rate.
	 * Max charge amount is 100.
	 * Affects the damage dealt to the target.
	 */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float ChargeAmount;

	/**
	 * The rate, in which the charge amount increases per second.
	 * Defaults to 12.5, so the weapon is fully charged after 2 seconds.
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float ChargeRate;

	/** The projectile that this weapon shoots. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<ARPGProjectile> ProjectileToShoot;

	/** Offset added when spawning the projectile. */
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FVector ProjectileOffset;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float ProjectileDirectionOffset;

	/** Defines the maximum amount of ammunition of the weapon. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon", meta = (ClampMin = "0", ClampMax = "999"))
	int32 MaxAmmunition;

	/** Defines the current amount of ammunition the weapon has left to shoot. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "0", ClampMax = "999"))
	int32 CurrentAmmunition;

	FTimerHandle TimerHandle_ChargeShot;

	void StartCharging();

	/**
	 * Updates the current ammo amount.
	 * @param Amount Amount to update. Positive increases, negative decreases amount.
	 */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void UpdateAmmunitionAmount(int32 Amount);

	/** Animation montage to play when the weapon is getting sheathed. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UAnimMontage* EquipMontage;

	/** Animation montage to play when the weapon is getting drawn. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UAnimMontage* UnequipMontage;

public:
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void Shoot();

	/** Increase the charge amount every quarter second with the charge rate. */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void ChargeShot();

	/** Stop the charging and reset the charge amount. */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void StopChargeShot();

	/** Return the HeadShouldMultiplier of the weapon. */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	float GetHeadShotMultiplier() const;

	/** Returns the charge amount of the weapon. */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	float GetChargeAmount() const;
	
	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnFullyChargedSignature OnFullyCharged;
};
