// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RPGStaminaComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaDepletedReplenishedSignature, URPGStaminaComponent*, StaminaComp, bool, bIsMax);

UCLASS( ClassGroup=(RPG), meta=(BlueprintSpawnableComponent) )
class UTILITYAI_API URPGStaminaComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	URPGStaminaComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, Category = "StaminaComponent")
	float Stamina;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StaminaComponent")
	float DefaultStamina;

public:
	/**
	 * Updates the owners stamina value.
	 * Negative values decrease, positive increase the stamina.
	 * Broadcasts if stamina is fully depleted or fully replenished.
	 * @param Amount Amount to update the stamina.
	 */
	UFUNCTION(BlueprintCallable, Category = "StaminaComponent")
	void UpdateStamina(float Amount);

	/** Returns the current stamina amount. */
	UFUNCTION(BlueprintCallable, Category = "StaminaComponent")
	float GetStamina() const;

	/** Returns the default (maximum) stamina amount. */
	UFUNCTION(BlueprintCallable, Category = "StaminaComponent")
	float GetDefaultStamina() const;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnStaminaDepletedReplenishedSignature OnStaminaDepletedReplenished;
};
