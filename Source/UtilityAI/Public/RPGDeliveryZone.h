// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RPGDeliveryZone.generated.h"

class UBoxComponent;

UCLASS()
class UTILITYAI_API ARPGDeliveryZone : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ARPGDeliveryZone();

protected:
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UBoxComponent* OverlapComp;

	UFUNCTION()
	void HandleOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
