// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RPGWeapon.generated.h"

class USphereComponent;
class UCapsuleComponent;
class USkeletalMeshComponent;
class UDamageType;
class UParticleSystem;
class UAnimMontage;

UCLASS()
class UTILITYAI_API ARPGWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARPGWeapon();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

protected:
	/** Serves as the origin/ root of the weapon for attachment. */
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	USphereComponent* SphereComp;

	/** Simple collision of the sword. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	UCapsuleComponent* CapsuleComp;

	/** The mesh which represents the ranged weapon, e.g. a bow. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	USkeletalMeshComponent* SkeletalMeshComp;

	/** Damage type of the weapon. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<UDamageType> DamageType;

	/** The amount of damage the weapon deals */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float Damage;

	/** Effect to spawn when the capsule collision hits other characters. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* HitEffect;

	/** Effect to spawn when the capsule hits something. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* DefaultHitEffect;

	/** Animation montage to play when the weapon is getting sheathed. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UAnimMontage* EquipMontage;

	/** Animation montage to play when the weapon is getting drawn. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UAnimMontage* UnequipMontage;

	/** Animation montage to play when the character attacks. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UAnimMontage* AttackMontage;

	/** Defines the start of the line trace. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName StartTraceSocketName;
	
	/** Defines the end of the line trace. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName EndTraceSocketName;

	/** Weapon has something recently */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	bool bHasHit;

	/** Defines how many seconds to ignore additional hits. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float HitCooldown;

	FTimerHandle TimerHandle_ResetHasHit;

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void StartWeaponDamageTracing();

	void StartDamageCooldown();
};
