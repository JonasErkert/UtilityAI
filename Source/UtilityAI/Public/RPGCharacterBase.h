// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RPGCharacterBase.generated.h"

class UCameraComponent;
class USpringArmComponent;
class URPGStaminaComponent;
class URPGHealthComponent;
class ARPGRangedWeapon;
class ARPGWeapon;

UENUM(BlueprintType)
enum class ECombatMode : uint8
{
	ENone		UMETA(DisplayName = "None"),
	EStealth	UMETA(DisplayName = "Stealth"),
	EMelee		UMETA(DisplayName = "Melee"),
	ERanged		UMETA(DisplayName = "Ranged")
};

UCLASS()
class UTILITYAI_API ARPGCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARPGCharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	/** Calculate the direction the character does move forward or backward. */
	void MoveForward(float Value);

	/** Calculate the direction the character does move left or right. */
	void MoveRight(float Value);

	/**
	 * Switches between rotation modes:
	 * Rotate to camera.
	 * Free rotation.
	 */
	UFUNCTION(BlueprintCallable, Category = "Character")
	void SwitchRotationMode(bool bIsFreeRotation);

	/** This function redirects input based on the combat mode. */
	void LeftClickInputAction();

	/** This function redirects input based on the combat mode when the button is pressed. */
	void RightClickInputActionStart();

	/** This function redirects input based on the combat mode when the button is released. */
	void RightClickInputActionStop();

	/** This function redirects input based on the combat mode when the spacebar is pressed. */
	void SpacebarInputActionStart();

	/** This function redirects input based on the combat mode when the spacebar is released. */
	void SpacebarInputActionStop();

	/** This function redirects input based on the combat mode when shift is pressed. */
	void ShiftInputActionStart();

	/** This function redirects input based on the combat mode when shift is released. */
	void ShiftInputActionStop();

	/**
	 * Enter stealth combat mode.
	 * Start crouching.
	 * Draws knife.
	 * TODO: Adjust key bindings.
	 */
	UFUNCTION(BlueprintCallable, Category = "Character")
	void ToggleStealthMode();

	/**
	 * Enter melee combat mode.
	 * Draws sword and shield.
	 * TODO: Adjust key bindings.
	 */
	UFUNCTION(BlueprintCallable, Category = "Character")
	void ToggleMeleeMode();

	/**
	 * Stops crouching.
	 * Stops aiming.
	 * TODO: Adjust key bindings.
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	void OnEnterNoneMode();
	virtual void OnEnterNoneMode_Implementation();

	void SwitchToCombatState(ECombatMode CombatMode);

	/** Start running. Increases max walk speed. Consumes stamina. */
	void Run();

	/** Stop running. Resets max walk speed to default. Starts regenerating stamina. */
	void StopRunning();

	/**
	 * Reduces stamina in the stamina component.
	 * @param Amount Amount to reduce.
	 */
	void ConsumeStamina(float Amount);

	/** Calls ConsumeStamina with specific stamina consumption. */
	void ConsumeStaminaRun();

	/** Calls ConsumeStamina with specific stamina consumption. */
	void ConsumeStaminaCharge();

	/** Updates/ Regenerates the stamina amount with the specified amount */
	void RegenerateStamina();

	/** Calls RegenerateStamina every 0.05 seconds. */
	void RegenerateStaminaOverTime();

	UFUNCTION()
	void OnHealthChanged(URPGHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Character")
	void CharacterDied();

	/** Executes a light sword attack. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Weapon")
	void WeaponLightAttack();

	/** Executes a heavy sword attack */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Weapon")
	void WeaponHeavyAttack();

	/** Executes a shield block to reduces damage. */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
	void OnStartShieldBlock();
	virtual void OnStartShieldBlock_Implementation();

	/** Executes a shield block to reduces damage. */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
	void OnStopShieldBlock();
	virtual void OnStopShieldBlock_Implementation();

	/** Executes a small weapon attack. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Weapon")
	void SmallWeaponAttack();

	/**
	 * Attaches the dagger to the right hand.
	 * @param bEquip Unequips the dagger if false.
	 */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void EquipSmallWeapon(bool bEquip = true);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	bool bSmallWeaponEquipped;

	/**
	 * Attaches the sword to the left hand.
	 * @param bEquip Unequips the sword if false.
	 */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void EquipWeapon(bool bEquip = true);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	bool bWeaponEquipped;

	/**
	 * Attaches the shield to the left hand.
	 * @param bEquip Unequips the shield if false.
	 */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void EquipShield(bool bEquip = true);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	bool bShieldEquipped;

	/**
	 * Attaches the bow to the left hand.
	 * @param bEquip Unequips the bow if false.
	 */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void EquipRangedWeapon(bool bEquip = true);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	bool bRangedWeaponEquipped;

	/**
	 * Called when aiming has changed.
	 * Used to update UI like toggling the crosshair on/ off.
	 */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Character")
	void AimingChanged();

	/** Starts charging the ranged weapon which increases damage. */
	void ChargeRangedWeapon();

	/** Stops charging the ranged weapon and resets the charged amount. */
	void StopChargeRangedWeapon();

	/** Gets called when weapon is fully charged, then reduces stamina and resets stamina regeneration. */
	UFUNCTION()
	void OnChargeExhaustion(ARPGRangedWeapon* RangedWeapon);

	/** Gets called when the stamina is fully depleted. */
	UFUNCTION()
	void OnStaminaDepletedReplenished(URPGStaminaComponent* OwningStaminaComp, bool bIsMax);

	/** Starts the camera shake. */
	void ShakeCamera();

	/** Moves the camera to the right of the character and closer for easier aiming. */
	void MoveCamToAimPos(float DeltaTime);

	/** Moves the camera to the default position of the camera, which is behind the character */
	void MoveCamToDefaultPos(float DeltaTime);

	/** Returns the ranged weapon which was spawned and attached at begin play. */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	ARPGRangedWeapon* GetRangedWeapon() const;

	/** Called after the combat mode has been set to a new one. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Character")
	void CombatModeChanged();

	/** The current combat state the character has activated. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character")
	ECombatMode CurrentCombatMode;

	/** Defines the maximum speed a character can move at. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	float MaxRunningSpeed;

	float MaxWalkSpeedDefault;

	/** Defines the maximum crouch speed. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	float MaxCrouchSpeed;

	float MaxCrouchSpeedDefault;

	/** Defines the stamina consumption rate while the character is running. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character", meta = (ClampMin = "0.0", ClampMax = "100.0"))
	float RunningStaminaConsumption;

	/** Defines the stamina consumption rate while the character has a fully charged ranged weapon. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character", meta = (ClampMin = "0.0", ClampMax = "100.0"))
	float FullyChargedStaminaConsumption;

	/** Defines the stamina regeneration rate while the character is idle/ walking. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character", meta = (ClampMin = "0.0", ClampMax = "100.0"))
	float IdleStaminaRegeneration;

	FTimerHandle TimerHandle_RunningStaminaConsumption;

	FTimerHandle TimerHandle_FullyChargedStaminaConsumption;

	FTimerHandle TimerHandle_StaminaRegeneration;

	/** Third person camera. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp;

	/** Spring arm the camera is attached to. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* SpringArmComp;

	/** Health component of the character. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	URPGHealthComponent* HealthComp;
	
	/** Stamina component of the character. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	URPGStaminaComponent* StaminaComp;

	/**
	 * Specifies the character yaw rotation rate when turning.
	 * Defaults to 540.
	 * @param Rate The rate the player turn to the new direction.
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character")
	float RotationRate;

	/** Pawn died previously. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	bool bDied;

	UPROPERTY(BlueprintReadOnly, Category = "Weapon")
	ARPGRangedWeapon* CurrentRangedWeapon;

	/** Ranged weapon BP the character can equip. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<ARPGRangedWeapon> EquippedRangedWeapon;

	UPROPERTY(BlueprintReadWrite, Category = "Weapon")
	ARPGWeapon* CurrentWeapon;

	/** Weapon BP the character can equip. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<ARPGWeapon> EquippedWeapon;

	UPROPERTY(BlueprintReadOnly, Category = "Weapon")
	ARPGWeapon* CurrentSmallWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<ARPGWeapon> EquippedSmallWeapon;

	/** Quiver to attach to the back. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<AActor> EquippedQuiver;

	AActor* CurrentQuiver;

	/** Shield to attach to the right hand and to the back. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<AActor> EquippedShield;

	UPROPERTY(BlueprintReadOnly, Category = "Weapon")
	AActor* CurrentShield;

	/** Name of the left hand socket a weapon is attached to. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName LeftHandSocketName;

	/** Name of the socket the shield is attached to. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName HandShieldSocketName;

	/** Name of the right hand socket a weapon is attached to. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName RightHandSocketName;

	/** Name of the socket the quiver is attached to on the back. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName QuiverSocketName;

	/** Name of the socket the bow is attached to on the back. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName BowSocketName;

	/** Name of the socket the sword is attached to on the side. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName SwordSocketName;

	/** Name of the socket the dagger is attached to on the back. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName DaggerSocketName;

	/** Name of the socket the shield is attached to on the back. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName ShieldSocketName;

	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	TSubclassOf<UCameraShake> ShootCameraShake;

	/** The interpolation speed of the camera positions. Higher is faster. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
	float CamTransitionSpeed;

	/** The camera location, relative to the spring arm position. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
	FVector AimCamPosition;

	FVector DefaultCamPosition;

	FVector CurrentCamPosition;

	bool bShouldTransition;

	/** True, if the character is aiming. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	bool bAiming;

	/** True, if the character is blocking with a shild. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	bool bIsShieldBlocking;

	bool bMoveToAimPosition;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Objective")
	bool bIsCarryingObjective;

	/** Returns the camera component of the character. */
	FORCEINLINE UCameraComponent* GetCameraComp() const { return CameraComp; }

	/** Returns the spring arm component the camera is attached to. */
	FORCEINLINE USpringArmComponent* GetSpringArmComp() const { return SpringArmComp; }

	/** Returns the location of the third person camera. */
	virtual FVector GetPawnViewLocation() const override;

	/**
	 * Enter ranged combat mode.
	 * Draws bow.
	 * TODO: Adjust key bindings.
	 */
	UFUNCTION(BlueprintCallable, Category = "Character")
	void ToggleRangedMode();

	/**
	 * Enables the transition of the camera to the aiming position.
	 * Starts charging the weapon.
	 */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void StartAiming();

	/**
	 * Enables the transition of the camera back to the default position.
	 * Stops charging the weapon.
	 */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void StopAiming();

	/** Shoots a projectile from the equipped ranged weapon. */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void ShootRangedWeapon();
};
