// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RPGProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class UDamageType;
class UParticleSystem;
class ARPGRangedWeapon;

UCLASS()
class UTILITYAI_API ARPGProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARPGProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile")
	USphereComponent* SphereComp;

	/** The mesh representation of the projectile, e.g. an arrow. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Projectile")
	UStaticMeshComponent* ProjectileMeshComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Projectile")
	UProjectileMovementComponent* ProjectileMovement;

	/** The initial speed the projectile has when shot. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile")
	float InitialSpeed;

	/** The maximum speed the projectile can travel. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile")
	float MaxSpeed;

	/** The lifespan of the projectile if it hits nothing withing the specified time. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile", meta = (ClampMin = "0.0", ClampMax = "100.0"))
	float LifeSpan;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile")
	TSubclassOf<UDamageType> DamageType;

	/** Effect to play when projectile hits something. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile")
	UParticleSystem* DefaultImpactEffect;

	/** Effect to play when projectile hits flesh. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile")
	UParticleSystem* FleshImpactEffect;

	/** Base damage the weapon deals when hitting a target. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
	float BaseDamage;

	float CalculatedDamage(bool bIsHeadshot);

public:
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Return the SphereComp subobject. */
	FORCEINLINE USphereComponent* GetSphereComp() const { return SphereComp; }

	/** Return the ProjectileMovement subobject. */
	FORCEINLINE UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	/**
	 * The weapon which spawned this projectile.
	 * Necessary to get charge amount to calculate damage.
	 */
	ARPGRangedWeapon* ProjectileSpawnerWeapon;

	/** The spawn location of the projectile to calculate the projectile (e.g. arrow) rotation when the collision sphere hits something. */
	FVector SpawnedAtLocation;
};
