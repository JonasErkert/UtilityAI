// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RPGGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UTILITYAI_API ARPGGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ARPGGameModeBase();

	void CompleteMission(APawn* InstigatorPawn);

	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void OnMissionCompleted(APawn* InstigatorPawn);

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void GameOver();

	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void OnPlayerDeath();
};
