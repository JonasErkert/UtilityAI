// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

// Surface impact types
#define SURFACE_IMPACT_DEFAULT		SurfaceType1
#define SURFACE_IMPACT_FLESH		SurfaceType2

// Custom collision trace channels
#define COLLISION_WEAPON			ECC_GameTraceChannel1

// Custom collision object channels
#define COLLISION_PROJECTILE		ECC_GameTraceChannel2
